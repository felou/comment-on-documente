# texte d'intention
10 000 signes (2 pages recto/verso)  
contexte du stage + choix formels, méthodologiques et conceptuels + lien entre lieu de stage et projet scientifique

## Contexte(s) du stage
Mon stage s'est déroulé dans le collectif associatif Luuse. Luuse travaille le medium numérique pour produire des outils d'édition, d'écriture, de publication et d'archive. Leurs outils sont utilisés dans le cadre de structures associatives ou culturelles ; dans le cadre pédagogique ; ainsi que par des artistes et créateur.ices dans leur travail. Iels cultivent une approche expérimentale et critique, fortement rattachée philosophiquement et politiquement à la culture libre, dont iels reprennent tant les valeurs que les outils.  

Pendant mon stage, j'ai tout particulièrement intégré deux projets de recherche par Luuse, dans le cadre d'un axe de recherche auto-initié appelé "Formes d'archives".  
Le premier est la conception et fabrication d'un numérisateur de livre non-destructif (bookscanner). Ce projet a pour base une documentation déjà existante, partagée en ligne au sein d'une communauté d'amateur.ices. Un de nos objectifs est de s'assurer que l'objet final soit facilement démontable et transportable afin d'être utilisé dans le cadre d'ateliers. Pour cette raison, ainsi que par adhésion aux pratiques de diffusion qui existent au sein de la communauté d'amateur.ices de bookscanner, il était important pour nous de documenter et rendre accessibles les plans et la documentation de notre projet.  
Le deuxième projet dans lequel je me suis investi est la conception et la production d'un outil de bibliothèque numérique pirate. Cette bibliothèque repose sur un serveur accessible à travers un réseau local. Ce projet demandait de mettre en place et de gérer une base de données, et de concevoir une interface de consultation. L'interface que nous produisons est pensée pour une première implémentation de l'outil dans Maxima, le lieu d'occupation temporaire dans lequel Luuse a ses bureaux. L'outil final vise également à être récupéré  partiellement ou totalement à différents niveaux de la machine. Pour cela, nous mettons en place une documentation des différents éléments qui le composent.  
Maxima est un lieu qui se situe à Forest, mis en place et géré par l'association Communa, dans une démarche d'autogestion. Dans ce cadre, j'ai pu participé au développement d'un outil générant une affiche/flyer de programmation du lieu, une contribution de Luuse au fonctionnement de Maxima. Cet outil doit permettre aux habitant.es de Maxima de générer l'affiche/flyer en remplissant les informations des évènements dans un tableur, sans avoir à entrer dans le code. Ce principe demande également un travail de documentation et de guide pour assurer l'autonomie de l'outil.  

<!-- Ces projets de recherche ont pour objectif de développer des outils réutilisables dans divers contextes (en particulier pédagogique), pour permettre une rémunération. Ils sont parallèle à des projets de commande pour lesquels Luuse est rémunéré par un.e client.e.   -->

J'ai également eu l'occasion de découvrir en étant à Bruxelles le réseau de chercheur.euses praticien.nes duquel Luuse est proche. J'ai participé à un premier atelier du projet ATNOFS (initié par divers collectifs dont le collectif Varia, à Rotterdam). Ce projet s'attelle à définir ce que peut être un serveur informatique à travers une perspective féministe intersectionnelle. L'atelier fut l'occasion de rencontrer les réflexions qui travaillent ces chercheur.euses, leurs pratiques et méthodologie de recherche. J'ai intégré certaines observations que j'ai pu faire durant cet atelier à ce rapport de stage, dans la mesure où la démarche de recherche et les enjeux abordés étaient similaires à la pratique de Luuse.

Pour construire ce rapport de stage, je me suis intéressé aux pratiques et aux enjeux de la documentation, dans ces contextes spécifiques.

## Définir la documentation
Pendant mon stage, j'ai observé les pratiques de documentations qui se mettaient en place par des moyens tant techniques que humains. Dans le contexte de Luuse, ces pratiques reposent en grande partie sur des outils numériques comme Git. Git est un logiciel utilisé pour développer collectivement des outils numériques, afin de gérer les différentes versions du code et de synchroniser le travail entre plusieurs ordinateurs. La documentation peut également passer par l'annotation, l'écriture ou le croquis. Quelque soit l'outil utilisé, la documentation engage des moyens humains au sens d'une organisation de plusieurs personnes autour d'un support physique qui accueille les échanges.  

Par ailleurs, la documentation a une importance particulière vis-à-vis de la culture libre dans laquelle Luuse s'inscrit. Les valeurs de la culture libre repose sur le développement d'outils ouverts, modifiables et distribuables librement ainsi que la mise en commun de savoirs et de productions culturelles.  
Parce qu'elle constitue un support d'échanges humains, la documentation est essentielle au développement d'outils libres. Pour ce qui est des outils numériques, elle permet d'agrémenter la complexité abstraite d'un code par du texte et des représentations. Elle donne un point d'accroche à une personne extérieure.

J'ai donc cherché à identifié les formes que pouvait prendre la documentation et les enjeux qu'elles recouvraient, dans les démarches qui reposent sur les valeurs du libre.

<!-- hypothèses de recherche -->
À partir de mes observations, j'ai identifié trois enjeux principaux. Tout d'abord, la documentation occupe une place essentielle pour autoriser la réappropriation d'un outil par une personne extérieure (non auteure). Par ailleurs, la documentation est un élément structurant du travail collectif. Elle est un support d'échange entre les acteur.ices du projet et permet l'identification des individualités. Enfin, la documentation telle qu'elle s'inscrit dans une démarche de recherche expérimentale constitue un support de restitution. Elle archive le processus de recherche, et par un travail de mise en lisibilité, le présente et le synthétise.

## choix méthodologiques et formels
Pour restistuer les matériaux d'observation que j'ai rassemblé pendant mon stage, j'ai choisi de réutiliser des méthodes propres à la pratique du numérique que j'ai découvert avec Luuse. Ces méthodes consistent à transférer des ensembles de données d'un outil à l'autre pour les afficher sous divers angles. Ce choix m'a paru pertinent pour présenter par la forme les pratiques que j'ai observé pendant mon stage et les logiques de pensées qui s'y rattachent.  
J'ai structuré ce rapport de stage sur un dépot Git (un dossier rassemblant l'ensemble des fichiers d'un projet). La plateforme Gitlab (qui héberge le logiciel Git) m'a permis de générer un site web à partir du contenu de ce dépot. Ce site est organisé sur la base des trois enjeux identifiés et décrits plus hauts.  
Enfin, le template de ce site web a été produit avec l'outil Mkdocs qui est spécialement prévu pour rassembler des éléments de documentations sur un template préconçu et modifiable.

## Les pratiques observées
Les pratiques de la documentation que j'ai observé prennent des formes variées, qui correspondent à des usages que je décris ci-dessous.

### Commenter
Le commentaire est un texte intégré dans le code et différencié du reste par des signes typographiques propre à chaque langage. Par exemple, un commentaire en langage html est encadré des signes "<!-- -->". L'usage le plus répandu du commentaire dans un code fini consiste à expliquer la machine à l'endroit même où elle se trouve. Cela permet à une personne qui ignore tout du code de comprendre la structure de l'objet et la fonction de chaque partie. Dans un moment de prise en main du langage Javascript, j'ai utilisé les commentaires pour détailler à l'écrit au sein du code ce que j'étais en train de produire.

Par ailleurs, une pratique du commentaire en cours de développement consiste à mettre un morceau de code de côté. En l'encadrant des signes typographiques appropriés, on désactive cette partie du code. Cela permet de demander temporairement à l'ordinateur d'ignorer cette partie, tout en la conservant à l'endroit où elle pourrait encore être utile.

L'outil Git engage la production d'une autre forme de commentaire, au sens d'un texte qui accompagne en se plaçant à côté d'un objet. Le fichier ReadMe.md, généré automatiquement lors de la création d'un dépot Git, a une fonction de page d'accueil. Le ReadMe explique à quoi sert le projet, comment il est structuré et comment l'utiliser.  
Par ailleurs, lors d'une contribution à un projet (un commit), Git demande d'intégrer un message dans lequel il faut expliquer ce que contient cette contribution. Le soin apporté à ce message permet de mieux visualiser l'avancement du projet et éventuellement de revenir sur l'historique des modifications.

### Organiser et nommer
La façon dont on nomme et on organise les fichiers et les dossiers a une fonction de documentation, car elle permet de préciser directement la fonction de chaque élément.

J'ai observé l'intérêt d'une bonne organisation et d'une dénomination pertinente des éléments d'un code. Cela permet aussi à une personne qui voudrait récupérer un élément unique d'une machine de faire le tri entre ce qui est utile de garder ou pas.

Au sein du code, le nom qu'on donne à des éléments comme les variables ou les classes impacte la compréhension, et la facilité qu'on peut avoir à s'y référer.

### Prendre des notes
L'usage de pads comme support d'écriture collective et minimaliste est un moyen de documentation de moments de réunions et d'échanges. Le pad est un support de prise des notes qui différencie les auteur.ices du texte par des couleurs. Une fonction d'historique permet de revenir en arrière.

J'ai observé que l'usage des pads avait une fonction essentielle au sein de Luuse ; ainsi que pendant l'atelier du projet ATNOFS. Comme objets de références, ils permettaient d'organiser la pensée et le travail collectif par le texte. Durant l'atelier ATNOFS, il fut également question de la façon dont il était possible de restituer le contenu des pads, par un outil de génération de fichiers PDF.

### Rendre visible
À plusieurs moments, nous avons eu l'usage de diagrammes et de visualisation pour produire un support d'échange. Ces supports avaient pour fonction de rendre concret et palpable un cheminement sur une interface ou l'organisation de concepts entre eux.

## Conclusion
Construire mon rapport de stage autour des pratiques de documentation m'a permis de comprendre leurs enjeux au regard des valeurs politiques portées par Luuse et les pratiques de la culture libre en général. Il me paraît essentiel d'intégrer cette approche de la documentation dans mon travail à venir. Il s'agit à la fois d'un engagement pour la mise en commun des outils et des ressources ; et de pratiques essentielles à une démarche de recherche expérimentale.

Vis-à-vis de mon projet de recherche, mon stage m'a apporté des compétences techniques de code mais également les modes de pensée et les méthodes de travail qui s'y rattachent.  
Mon projet de recherche s'intéresse à la mise en narration des outils numériques pour permettre leur réappropriation technique et politique. J'ai décidé pour cela de produire un outil de narration expérimentale sur la base du logiciel Git. En comprenant les logiques de manipulation des données, de récupération et de réadaptation d'outils, j'ai pu envisager avec plus de clarté les possibilités expérimentales de ce projet.  
D'un point de vue méthodologique, j'ai également pu prendre en main et expérimenter la démarche de conception d'une interface graphique : réfléchir au parcours de l'utilisateur.ice dans l'outil, penser un principe d'affichage des données à l'écran. Je pourrais également récupérer ces éléments de méthodes pour mon projet de recherche.  
Enfin le contexte bruxellois m'a permis de rencontrer des collectifs et des lieux qui travaillent sur des enjeux similaires, que ce soit dans une démarche de recherche expérimentale ou conceptuelle, ou dans des cadres associatifs et pédagogiques.

<!--
La documentation en informatique a un rôle essentiel. 
Commenter un code : Il s'agit d'expliquer la machine à l'endroit même où elle se trouve. Cela permet à plusieurs personnes de travailler sur la même machine et de récupérer le code de quelqu'un.e d'autre en ayant une idée plus précise de ce qu'il fait. Quand un code n'est pas documenté, c'est plus dur de rentrer dedans.

## enjeux principaux :
- l'archivage du travail et du processus de recherche pour objectif de mémoire ;
- les modalités de mise en commun des outils par la documentation ;
- (quelque chose comme) la reconnaissance des individualités par l'identifications : noms/couleurs sur le pads.
- autre ?

## les objets d'études possibles :
- les pads : historique, noms/couleurs/identifications, modes d'écritures
- commentaires dans le code
- git : commits (+??)
- ... ?

## évaluation oral
- Contextualisation : technologique, socioculturel, historique, dimension esthétique et heuristique (??) de mon approche
- expression : cohérence fond/forme, choix méthodologiques et pratiques appliqués.
- la suite : césure, pratique d'atelier / transmission de savoirs, à partir d'outils designés. -->